

/**
 *
 * Esta clase está hecha especificamente para servir como modelo del objeto a utilizar para
 * representar las posiciones de memoria o tripletas para una posterior traducción a un código de tres direcciones.
 * @author calv3
 */
public class Memoria {
    private String p0,p1; // se podría decir que son las posiciones de memoria que guardan un valor cada uno
    private char operacion; //es el caracter representativo de la operación a hacer
    private float resultado; //se almacena el valor caracteristico de la operación hecha respecto a p0 con p1 usando 'operacion'
    private int pos; //indica la 'posicion' de memoria
    private int optimizado; //servirá de referencia para indicar el número de barrido de optimización del código.
    
    
    //Este es el constructor el cual se inicializará en cero las variables numéricas
    //  en cadena vacía en caso del String, denotando que no significa que sea NULL
    //  en caso del Char es forzoso poner un caracter, en este caso un espacio  
    public Memoria(){
        pos = optimizado = 0;
        resultado = 0;
        operacion = ' ';
        p0 = p1 = "";
    }

    
    //Constructor hecho para hacer más fácil la creación del separador de memoria
    public Memoria(String p0, char operacion){
        pos = optimizado = 0;
        resultado = 0;
        this.operacion = operacion;
        this.p0 = p0;
        p1 = "";
    }
    
    public String getP0() {
        return p0;
    }

    public void setP0(String p0) {
        this.p0 = p0;
    }

    public String getP1() {
        return p1;
    }

    public void setP1(String p1) {
        this.p1 = p1;
    }

    public char getOperacion() {
        return operacion;
    }

    public void setOperacion(char operacion) {
        this.operacion = operacion;
    }

    public float getResultado() {
        return resultado;
    }

    public void setResultado(float resultado) {
        this.resultado = resultado;
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    
    
}
