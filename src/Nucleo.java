
import java.util.ArrayList;
import java.util.Scanner;
import java.util.StringTokenizer;
import javax.swing.JOptionPane;
//<editor-fold desc="Esto es solo un.. Agregao tio">

/**
 * XD espero le entiendan al código báse y así es como se maneja de forma simple
 * lo de la recursividad y el como se cambia de un estado a otro, empezando con
 * f0 y de ahi hacia adelante
 *
 */
//1E+5
//0.5E-8
//public class Nucleo extends DeString {
public class Nucleo {

    
    private static DeString ds;
    /**
     * Servirá para guardar las variables declaradas
     */
    private static ArrayList Variables;
    /**
     * Contador para contar condiciones SiSoloSi
     */
    private static int cS;
    /**
     * Contador para contar condiciones Urgido
     */
    private static int cU;
    /**
     * Contador para contar condiciones SiNo
     */
    private static int cSS;
    /**
     * Contador para contar condiciones Disponible
     */
    private static boolean cD;
    /**
     * Contador para contar condiciones Disponible para estar en f9
     */
    private static boolean cD2;
    /**
     * Contador para contar condiciones Disponible para estar en f9
     */
    private static boolean bOperador;
    /**
     * Contador para indicar en que línea está el "carro"
     */
    private static int cL;
    /**
     * Contador para indicar en que columna está del "carro"
     */
    private static int cC;
    /**
     * Contador para indicar pos en la linea de texto
     */
    private static int cCC;
    /**
     * Contador para auxiliar en fase de pruebas
     */
    private static int nAux;
    /**
     * Contador para indicar partes de de concatenación o vecces que para en
     * f156
     */
    private static int cStr;
    /**
     * Variable auxiliar para guardar el nombre y valor de la variable
     */
    private static String strAux;
    /**
     * Variable auxiliar para Concatenar texto para la función SACALA
     */
    private static String strAux2;
    /**
     * Variable auxiliar para Concatenar texto para la función SACALA
     */
    private static String strAux3;
    /**
     * Lector de entrada de teclado
     */
    private static Scanner scn;

    public static void main(String[] vector) {
        //Inicializamos Variables del Compilador
        Scanner scn = new Scanner(System.in);
        Variables = new ArrayList();
        cS = 0;
        cSS = 0;
        cU = 0;
        cL = 0;
        cC = 0;
        cD = false;
        nAux = 0;
        strAux = "";
        ds = new DeString();
        //Mandamos a inicializar el proceso con F0
        //  en caso de ser devuelto un 1 al final del código
        //  se habrá compilado correctamente
        //  caso contrario, revisamos el tipo de error
        //  para ello guardamos el valor que regresa
        //  para comparar despues
        int code = f0("calienta.. y te vienes");
        if (code == 1) {
            //System.out.println("TODO ESTA PERFECTO");
        } else {
            //////System.out.println("ALGO FALLO...");
            //////System.out.println("Codigo ");
        }
    }

    /**
     * @return the cS
     */
    public static int getcS() {
        return cS;
    }

    /**
     * @return the cU
     */
    public static int getcU() {
        return cU;
    }

    /**
     * @return the cSS
     */
    public static int getcSS() {
        return cSS;
    }

    /**
     * @return the cD
     */
    public static boolean iscD() {
        return cD;
    }

    /**
     * @return the cL
     */
    public static int getcL() {
        return cL;
    }

    /**
     * @return the cC
     */
    public static int getcC() {
        return cC;
    }

    /**
     * @return the nAux
     */
    public static int getnAux() {
        return nAux;
    }

    /**
     * @return the strAux
     */
    public static String getStrAux() {
        return strAux;
    }

    /**
     * @return the strAux2
     */
    public static String getStrAux2() {
        return strAux2;
    }

    /**
     * @return the cCC
     */
    public static int getcCC() {
        return cCC;
    }

    /**
     * @return the Variables
     */
    public static ArrayList getVariables() {
        return Variables;
    }

    public Nucleo() {
        ds = new DeString();
        Scanner scn = new Scanner(System.in);
        Variables = new ArrayList();
        cS = 0;
        cSS = 0;
        cU = 0;
        cL = 0;
        cC = 0;
        cCC = 0;
        cD = false;
        bOperador = false;
        nAux = 0;
        strAux = "";
        strAux2 = "";
        strAux3 = "";
    }

    public static void Activa(){
        ds = new DeString();
    }
    
    
    public static int f0(String cod) {
        //////System.out.println(" inicia ");
        //Desde aquí pueden notar que estoy agregando bloques de TRY-CATCH
        //  esto con la intención de atrapar un error común, dado sea el caso
        //  para cuando ya se acabo el código y mande el error -1
        //  también preveerá ese apartado de que no ha terminado bien el bloque
        //  de código calienta. {CÓDIGO}  ..Y te vienes

        Scanner scn = new Scanner(System.in);
        Variables = new ArrayList();
        cS = 0;
        cSS = 0;
        cU = 0;
        cL = 1;
        cC = 1;
        cCC = 0;
        cD = false;
        bOperador = false;
        nAux = 0;
        strAux = "";
        strAux2 = "";
        strAux3 = "";
        try {
            cC++;
            int pos = 0;
            if (cod.charAt(pos) == ' ' || cod.charAt(pos) == '\t' || cod.charAt(pos) == '\r') {
                return f0(cod, ++pos);
            }
            if (cod.charAt(pos) == '\n') {
                //cC -= 2;
                salto();
                return f0(cod, ++pos);
            }
            if (cod.charAt(pos) == 'c') {
                return f1(cod, ++pos);
            }
            return -4;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f0(String cod, int pos) {
        try {
            cC++;
            cCC++;
            if (cod.charAt(pos) == ' ' || cod.charAt(pos) == '\t' || cod.charAt(pos) == '\r') {
                return f0(cod, ++pos);
            }
            if (cod.charAt(pos) == '\n') {
                //cC -= 2;
                salto();
                return f0(cod, ++pos);
            }
            if (cod.charAt(pos) == 'c') {
                return f1(cod, ++pos);
            }
            return -4;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f1(String cod, int pos) {
        try {
            cC++;
            cCC++;
            if (cod.charAt(pos) == 'a') {
                return f2(cod, ++pos);
            }
            return -4;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f2(String cod, int pos) {
        try {
            cC++;
            cCC++;
            if (cod.charAt(pos) == 'l') {
                return f3(cod, ++pos);
            }
            return -4;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f3(String cod, int pos) {
        try {
            cC++;
            cCC++;
            if (cod.charAt(pos) == 'i') {
                return f4(cod, ++pos);
            }
            return -4;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f4(String cod, int pos) {
        try {
            cC++;
            cCC++;
            if (cod.charAt(pos) == 'e') {
                return f5(cod, ++pos);
            }
            return -4;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f5(String cod, int pos) {
        try {
            cC++;
            cCC++;
            if (cod.charAt(pos) == 'n') {
                return f6(cod, ++pos);
            }
            return -4;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f6(String cod, int pos) {
        try {
            cC++;
            cCC++;
            if (cod.charAt(pos) == 't') {
                return f7(cod, ++pos);
            }
            return -4;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f7(String cod, int pos) {
        try {
            cC++;
            cCC++;
            if (cod.charAt(pos) == 'a') {
                return f8(cod, ++pos);
            }
            return -4;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f8(String cod, int pos) {
        try {
            cC++;
            cCC++;
            if (cod.charAt(pos) == '.') {
                return f9(cod, ++pos);
            }
            return -4;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    // Inicia el reconocimiento para las palabras reservadas
    private static int f9(String cod, int pos) {
        //////System.out.print("F9 ");
        ////System.out.println(Variables.size());
        cC++;
        if (cod.charAt(pos) == ' ' || cod.charAt(pos) == '\t' || cod.charAt(pos) == '\r') {
            cCC++;
            return f9(cod, ++pos);
        }
        if (cod.charAt(pos) == '\n') {
            salto();
            return f9(cod, ++pos);
        }

        if (cod.charAt(pos) == '?') {
            if (cod.charAt(pos + 1) == '?') {
                cC += 2;
                cCC += 2;
                pos++;
                while (cod.charAt(pos) != '\r') {
                    cC++;
                    cCC++;
                    pos++;
                }
                return f9(cod, ++pos);
            } else {
                return -18;
            }
        }
        String valida = cod.substring(pos, pos + 10);
        //Suponiendo que tenemos una palabra reservada, la buscamos, adentro de los siguientes 10 caracteres
        if (cU == 0 || (cU > 0 && cD)) {
            if (valida.contains("enpieza") || valida.contains("entera") || valida.contains("metela") || valida.contains("sacala") || valida.contains("sisolosi") || valida.contains("te brinco") || valida.contains("testa") || valida.contains("urgido")) {
                if (valida.contains("enpieza")) {
                    ////System.out.println("enpieza");
                    cC += 7;
                    cCC += 7;
                    return f36(cod, pos + 7);
                }
                if (valida.contains("entera")) {
                    ////System.out.println("entera");
                    cC += 6;
                    cCC += 6;
                    return f54(cod, pos + 6);
                }
                if (valida.contains("metela")) {
                    ////System.out.println("metela");
                    cC += 6;
                    cCC += 6;
                    return f105(cod, pos + 6);
                }
                if (valida.contains("sacala")) {
                    ////System.out.println("sacala");
                    cC += 6;
                    cCC += 6;
                    return f155(cod, pos + 6);
                }
//            if (valida.equals("sisolosi")) {
//                return f170(cod, pos + 8);
//            }
                if (valida.contains("te brinco")) {
                    ////System.out.println("te brinco");
                    cC += 9;
                    cCC += 9;
                    return f200(cod, pos + 9);
                }
                if (valida.contains("testa")) {
                    ////System.out.println("testa");
                    cC += 5;
                    cCC += 5;
                    return f244(cod, pos + 5);
                }
                if (valida.contains("urgido")) {
                    ////System.out.println("urgido");
                    cC += 6;
                    cCC += 6;
                    return f385(cod, pos + 6);
                }
            }
        }
        if (!cD && !cD2 && cod.substring(pos, pos + 10).contains("disponible")) {
            //////System.out.print("Disponible ");
            cC += 10;
            cCC += 10;
            cD2 = true;
            return f429(cod, pos += 10);
        }
        if (cD2 && cod.substring(pos, pos + 9).contains("..no pudo")) {
            cC += 9;
            cCC += 9;
            cD2 = false;
            cD = false;
            return f9(cod, pos += 9);
        }
        if (cod.charAt(pos) == '.') {
            //Con la función valida, revisamos que las banderas estén apagadas * estaba implementando algo más pesado pero de momento servirá para la causa
            //      *es referente a que si está abierto un bloque Sisolosi/Sino/Urgido
            //////System.out.println("---" + cod.substring(pos, pos + 9));
            if (cU > 0 && cod.substring(pos, pos + 2).contains("..")) {
                if (cD || cD2) {
                    return -11;
                }
                cU--;
                cC += 2;
                cCC += 2;
                return f9(cod, pos += 2);
            }
            if (valida()) {
                return f3000(cod, ++pos);
            }
        }

        //Aqui empeze a modificar
        String var = "";
        while (cod.charAt(pos) != ' ' && cod.charAt(pos) != '\t' && cod.charAt(pos) != '\r') {
            cC++;
            cCC++;
            var += cod.charAt(pos++);
        }
        while (cod.charAt(pos) == ' ') {
            pos++;
            cC++;
            cCC++;
        }
        if (cod.charAt(pos) == '=') {
            ////System.out.println("Existe" + cod.charAt(pos));
            cCC++;
            cC++;
            ////System.out.println(var);
            if (ExisteVariable(var) > 0) {
                String valor = "";
                String valorAux = "";
                pos++;
                while (cod.charAt(pos) != ':' && cod.charAt(pos) != '\r') {
                    cCC++;
                    cC++;
                    ////System.out.println(cod.charAt(pos));
                    valor += cod.charAt(pos++);
                }
                ////System.out.println("----------");
                StringTokenizer st = new StringTokenizer(valor, "^*/+-():", true);
                valor = "";
                while (st.hasMoreTokens()) {
                    valorAux = st.nextToken();
                    valorAux = valorAux.trim();
                    ////System.out.println(valorAux);
                    try {
                        if (valorAux.indexOf("^") == -1 && valorAux.indexOf("*") == -1 && valorAux.indexOf("/") == -1 && valorAux.indexOf("+") == -1 && valorAux.indexOf("-") == -1 && valorAux.indexOf("(") == -1 && valorAux.indexOf(")") == -1 && valorAux.indexOf(":") == -1) {
                            switch (TipoVariable(valorAux)) {
                                case 1://Testa
                                    return -22;
                                case 2://Entera
                                    valorAux = ObtenValor(valorAux);
                                    break;
                                case 3://Enpieza
                                    valorAux = ObtenValor(valorAux);
                                    break;
                            }
                        }
                        valor += valorAux;
                    } catch (Exception error) {
                        valor += valorAux;
                    }
                }
                try {
                    ////System.out.println("***************");
                    ////System.out.println(cod.charAt(pos));
                    if (cod.charAt(pos) == ':') {
                        cC++;
                        cCC++;
                        int g = Guarda(var, valor);
                        ////System.out.println(g);
                        if (g > 0) {
                            return f9(cod, pos++);
                        }
                    } else {
                        return -12;
                    }
                } catch (Exception error) {
                    return -11;
                }
            } else {
                return -10;
            }
        } else {
            return -21;
        }
        return -1;
    }

    private static int f36(String cod, int pos) {
        ////System.out.println("36");
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == ' ' || cod.charAt(pos) == '\t') {
                return f37(cod, ++pos);
            }
            if (cod.charAt(pos) == '\r' || cod.charAt(pos) == '\n') {
                return -11;
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f37(String cod, int pos) {
        ////System.out.println("37");
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == ' ' || cod.charAt(pos) == '\t') {
                return f37(cod, ++pos);
            }
            if (Character.isLetter(cod.charAt(pos))) {
                strAux = "" + cod.charAt(pos);
                return f38(cod, ++pos);
            }

            if (cod.charAt(pos) == ':' || cod.charAt(pos) == '=') {
                return -15;
            }
            if (cod.charAt(pos) == '\r' || cod.charAt(pos) == '\n') {
                return -11;
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f38(String cod, int pos) {
        ////System.out.println("38");
        cC++;
        cCC++;
        try {
            if (Character.isLetterOrDigit(cod.charAt(pos))) {
                strAux += cod.charAt(pos);
                return f38(cod, ++pos);
            }
            if (cod.charAt(pos) == ' ' || cod.charAt(pos) == '\t') {
                return f39(cod, ++pos);
            }
            if (cod.charAt(pos) == ':') {
                getVariables().add(new enpieza(getStrAux()));
                strAux = "";
                return f9(cod, ++pos);
            }
            if (cod.charAt(pos) == '=') {
                getVariables().add(new enpieza(getStrAux()));
                strAux = "";
                return f40(cod, ++pos);
            }
            if (cod.charAt(pos) == '\r' || cod.charAt(pos) == '\n') {
                return -11;
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f39(String cod, int pos) {
        ////System.out.println("39");
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == ' ' || cod.charAt(pos) == '\t') {
                return f39(cod, ++pos);
            }
            if (cod.charAt(pos) == ':') {
                getVariables().add(new enpieza(getStrAux()));
                strAux = "";
                return f9(cod, ++pos);
            }
            if (cod.charAt(pos) == '=') {
                getVariables().add(new enpieza(getStrAux()));
                strAux = "";
                return f40(cod, ++pos);
            }
            if (cod.charAt(pos) == '\r' || cod.charAt(pos) == '\n') {
                return -11;
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f40(String cod, int pos) {
        ////System.out.println("40");
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == ' ' || cod.charAt(pos) == '\t') {
                return f40(cod, ++pos);
            }
            if (cod.charAt(pos) == ':' || cod.charAt(pos) == '\r' || cod.charAt(pos) == '\n') {
                return -7;
            }
            strAux += cod.charAt(pos);
            return f41(cod, ++pos);

        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f41(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == ':') {
                //DeString temp = new DeString(getStrAux());
                ds.nuevaOperacion(getStrAux());
                GuardarValor(ds.getResultadoEnString());
                //ds.setSeparadorMemoria(cod);
                strAux = "";
                return f9(cod, ++pos);
            }
            if (cod.charAt(pos) == '\r' || cod.charAt(pos) == '\n') {
                return -11;
            }
            if (cod.charAt(pos) == ' ' || cod.charAt(pos) == '\t') {
                return f42(cod, ++pos);
            }
            strAux += cod.charAt(pos);
            return f41(cod, ++pos);
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f42(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == ':') {
                ds.nuevaOperacion(getStrAux());
                GuardarValor(ds.getResultadoEnString());
                strAux = "";
                return f9(cod, ++pos);
            }
            if (cod.charAt(pos) == '\r' || cod.charAt(pos) == '\n') {
                return -12;
            }
            if (cod.charAt(pos) == ' ' || cod.charAt(pos) == '\t') {
                return f42(cod, ++pos);
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f54(String cod, int pos) {
        ////System.out.println("54");
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == ' ' || cod.charAt(pos) == '\t') {
                return f55(cod, ++pos);
            }
            if (cod.charAt(pos) == '\r' || cod.charAt(pos) == '\n') {
                return -11;
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f55(String cod, int pos) {
        ////System.out.println("55");
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == ' ' || cod.charAt(pos) == '\t') {
                return f55(cod, ++pos);
            }
            if (Character.isLetter(cod.charAt(pos))) {
                strAux += cod.charAt(pos);
                return f56(cod, ++pos);
            }

            if (cod.charAt(pos) == ':' || cod.charAt(pos) == '=') {
                return -16;
            }
            if (cod.charAt(pos) == '\r' || cod.charAt(pos) == '\n') {
                return -11;
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f56(String cod, int pos) {
        ////System.out.println("56");
        cC++;
        cCC++;
        try {
            if (Character.isLetterOrDigit(cod.charAt(pos))) {
                strAux += cod.charAt(pos);
                return f56(cod, ++pos);
            }
            if (cod.charAt(pos) == ' ' || cod.charAt(pos) == '\t') {
                return f57(cod, ++pos);
            }
            if (cod.charAt(pos) == ':') {
                getVariables().add(new entera(getStrAux()));
                strAux = "";
                return f9(cod, ++pos);
            }
            if (cod.charAt(pos) == '=') {
                getVariables().add(new entera(getStrAux()));
                strAux = "";
                return f59(cod, ++pos);
            }
            if (cod.charAt(pos) == '\r' || cod.charAt(pos) == '\n') {
                return -12;
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f57(String cod, int pos) {
        ////System.out.println("57");
        cC++;
        cCC++;
        try {

            if (cod.charAt(pos) == ' ' || cod.charAt(pos) == '\t') {
                return f57(cod, ++pos);
            }
            if (cod.charAt(pos) == ':') {
                getVariables().add(new entera(getStrAux()));
                strAux = "";
                return f9(cod, ++pos);
            }
            if (cod.charAt(pos) == '=') {
                getVariables().add(new entera(getStrAux()));
                strAux = "";
                return f59(cod, ++pos);
            }
            if (cod.charAt(pos) == '\r' || cod.charAt(pos) == '\n') {
                return -12;
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f59(String cod, int pos) {
        ////System.out.println("59");
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == ' ' || cod.charAt(pos) == '\t') {
                return f60(cod, ++pos);
            }
            if (cod.charAt(pos) == ':') {
                return -7;
            }
            if (cod.charAt(pos) == '\r' || cod.charAt(pos) == '\n') {
                return -12;
            }
            if (cod.charAt(pos) == '.' || cod.charAt(pos) == 'E') {
                return -8;
            }
            strAux += cod.charAt(pos);
            return f60(cod, ++pos);
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f60(String cod, int pos) {
        ////System.out.println("60");
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == ':') {
                //System.out.println(getStrAux());
                //////System.out.println(new DeString(getStrAux()).getResultadoEnString());
                //System.out.println(ds);
                ds.nuevaOperacion(getStrAux());
                GuardarValor(ds.getResultadoEnString());
                strAux = "";
                return f9(cod, ++pos);
            }
            if (cod.charAt(pos) == '\r' || cod.charAt(pos) == '\n') {
                return -12;
            }
            if (cod.charAt(pos) == '.' || cod.charAt(pos) == 'E') {
                return -8;
            }
            strAux += cod.charAt(pos);
            return f60(cod, ++pos);
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f100(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == 'e') {
                return f101(cod, ++pos);
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f101(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == 't') {
                return f102(cod, ++pos);
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f102(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == 'e') {
                return f103(cod, ++pos);
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f103(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == 'l') {
                return f104(cod, ++pos);
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f104(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == 'a') {
                return f105(cod, ++pos);
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f105(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == ' ' || cod.charAt(pos) == '\t') {
                return f105(cod, ++pos);
            }
            if (cod.charAt(pos) == '(') {
                return f106(cod, ++pos);
            }
            if (cod.charAt(pos) == '\r' || cod.charAt(pos) == '\n') {
                return -11;
            }
            return -6;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f106(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == ' ' || cod.charAt(pos) == '\t') {
                return f106(cod, ++pos);
            }
            if (Character.isLetter(cod.charAt(pos))) {
                strAux = cod.charAt(pos) + "";
                return f107(cod, ++pos);
            }
            if (cod.charAt(pos) == ')') {
                return -7;
            }
            if (cod.charAt(pos) == '\r' || cod.charAt(pos) == '\n') {
                return -11;
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f107(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (Character.isLetterOrDigit(cod.charAt(pos))) {
                strAux += cod.charAt(pos);
                return f107(cod, ++pos);
            }
            if (cod.charAt(pos) == ' ' || cod.charAt(pos) == '\t') {
                return f108(cod, ++pos);
            }
            if (cod.charAt(pos) == ')') {
                return f109(cod, ++pos);
            }
            if (cod.charAt(pos) == '\r' || cod.charAt(pos) == '\n') {
                return -11;
            }
            return -6;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f108(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == ' ' || cod.charAt(pos) == '\t') {
                return f108(cod, ++pos);
            }
            if (cod.charAt(pos) == ')') {
                return f109(cod, ++pos);
            }
            if (cod.charAt(pos) == '\r' || cod.charAt(pos) == '\n') {
                return -11;
            }
            return -6;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f109(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == ' ' || cod.charAt(pos) == '\t') {
                return f109(cod, ++pos);
            }
            if (cod.charAt(pos) == '\r' || cod.charAt(pos) == '\n') {
                return -11;
            }
            if (cod.charAt(pos) == ':') {

                boolean Encontrado = false;
                //Hacemos un barrido a todo lo que tenga la lista de VARIABLES
                //ya sea buscando por completo o se sale cuando lo encuentra
                for (int i = 0; i < getVariables().size() && !Encontrado; i++) {
                    //cada una de las variables lo guardamos en un objeto genérico
                    Object Variable = getVariables().get(i);
                    //el cual, después revisamos que tipo es; Testa/Entera/Enpieza
                    if (Variable instanceof testa) {
                        //en caso de ser instancia de "testa", lo guardamos en una variable "aux" de tipo testa haciendo un CAST
                        testa aux = (testa) Variable;
                        //ahora revisamos por el nombre de la variable para saber si es la que buscamos
                        if (aux.getNombre().equals(getStrAux())) {
                            //de ser así, entonces espera al ingreso de datos para la variable
                            aux.setValor(JOptionPane.showInputDialog("Ingresa para " + aux.getNombre()));
                            //se vuelve a guardar la variable que se asigno un nuevo VALOR
                            getVariables().set(i, aux);
                            //indicamos que ya se encontró para no seguir recorriendo la lista
                            Encontrado = true;
                        }
                    }//Lo mismo para las otras dos validaciones de entera o enpieza
                    if (Variable instanceof entera) {
                        entera aux = (entera) Variable;
                        if (aux.getNombre().equals(getStrAux())) {
                            aux.setValor(Integer.parseInt(JOptionPane.showInputDialog("Ingresa para " + aux.getNombre())));
                            getVariables().set(i, aux);
                            Encontrado = true;
                        }
                    }
                    if (Variable instanceof enpieza) {
                        enpieza aux = (enpieza) Variable;
                        if (aux.getNombre().equals(getStrAux())) {
                            aux.setValor(Float.parseFloat(JOptionPane.showInputDialog("Ingresa para " + aux.getNombre())));
                            getVariables().set(i, aux);
                            Encontrado = true;
                        }
                    }
                }
                if (!Encontrado) {
                    //No encontró la variable, enviamos un -10 de error
                    return -10;
                }
                return f9(cod, ++pos);
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    //sacala
    private static int f150(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == 'a') {
                return f151(cod, ++pos);
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f151(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == 'c') {
                return f152(cod, ++pos);
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f152(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == 'a') {
                return f153(cod, ++pos);
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f153(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == 'l') {
                return f154(cod, ++pos);
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f154(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == 'a') {
                return f155(cod, ++pos);
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f155(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == ' ' || cod.charAt(pos) == '\t') {
                return f155(cod, ++pos);
            }
            if (cod.charAt(pos) == '(') {
                strAux = "";
                return f156(cod, ++pos);
            }
            if (cod.charAt(pos) == '\r' || cod.charAt(pos) == '\n') {
                return -11;
            }
            return -6;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f156(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == ' ' || cod.charAt(pos) == '\t') {
                return f156(cod, ++pos);
            }
            if (cod.charAt(pos) == '"') {
                return f157(cod, ++pos);
            }
            if (Character.isLetter(cod.charAt(pos))) {
                strAux = cod.charAt(pos) + "";
                return f160(cod, ++pos);
            }
            if (cod.charAt(pos) == ')') {
                return -7;
            }
            if (cod.charAt(pos) == '\r' || cod.charAt(pos) == '\n') {
                return -11;
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f157(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == '"') {
                return f158(cod, ++pos);
            }
            if (cod.charAt(pos) == '\r' || cod.charAt(pos) == '\n') {
                return -11;
            }
            strAux += cod.charAt(pos);
            return f157(cod, ++pos);

        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f158(String cod, int pos) {
        cC++;
        cCC++;
        cStr++;
        try {
            if (cod.charAt(pos) == '+') {
                strAux2 += getStrAux();
                strAux = "";
                return f156(cod, ++pos);
            }
            if (cod.charAt(pos) == ' ' || cod.charAt(pos) == '\t') {
                return f158(cod, ++pos);
            }
            if (cod.charAt(pos) == ')') {
                strAux2 += getStrAux();
                strAux = "";
                return f159(cod, ++pos);
            }
            if (cod.charAt(pos) == '"' || Character.isLetterOrDigit(cod.charAt(pos))) {
                return -14;
            }
            if (cod.charAt(pos) == '\r' || cod.charAt(pos) == '\n') {
                return -11;
            }
            return -6;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f159(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == ' ' || cod.charAt(pos) == '\t') {
                return f159(cod, ++pos);
            }
            if (cod.charAt(pos) == ':') {
                //////System.out.print(getStrAux());
                strAux = "";
                return f9(cod, ++pos);
            }
            if (cod.charAt(pos) == '\r' || cod.charAt(pos) == '\n') {
                return -11;
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f160(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (Character.isLetterOrDigit(cod.charAt(pos))) {
                strAux += cod.charAt(pos);
                return f160(cod, ++pos);
            }
            if (cod.charAt(pos) == '+') {
                if (ExisteVariable(getStrAux()) > 0) {
                    strAux = ObtenValor(getStrAux());
                } else {
                    return -10;
                }
                strAux2 += getStrAux();
                strAux = "";
                return f156(cod, ++pos);
            }
            if (cod.charAt(pos) == ' ' || cod.charAt(pos) == '\t') {
                return f161(cod, ++pos);
            }
            if (cod.charAt(pos) == ')') {
                if (ExisteVariable(getStrAux()) > 0) {
                    strAux = ObtenValor(getStrAux());
                } else {
                    return -10;
                }
                strAux2 += getStrAux();
                strAux = "";
                return f162(cod, ++pos);
            }
            if (cod.charAt(pos) == '\r' || cod.charAt(pos) == '\n') {
                return -11;
            }
            return -6;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f161(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == ' ' || cod.charAt(pos) == '\t') {
                return f161(cod, ++pos);
            }
            if (cod.charAt(pos) == '+') {
                if (ExisteVariable(getStrAux()) > 0) {
                    strAux = ObtenValor(getStrAux());
                } else {
                    return -10;
                }
                strAux2 += getStrAux();
                strAux = "";
                return f156(cod, ++pos);
            }
            if (cod.charAt(pos) == ')') {
                return f162(cod, ++pos);
            }
            if (cod.charAt(pos) == '"' || Character.isLetterOrDigit(cod.charAt(pos))) {
                return -14;
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f162(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == ' ' || cod.charAt(pos) == '\t') {
                return f162(cod, ++pos);
            }
            if (cod.charAt(pos) == '\r' || cod.charAt(pos) == '\n') {
                return -12;
            }
            if (cod.charAt(pos) == ':') {
                return f9(cod, ++pos);
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    //sisolosi
    private static int f170(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == 'i') {
                return f170(cod, ++pos);
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f171(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == 's') {
                return f172(cod, ++pos);
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f172(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == 'o') {
                return f173(cod, ++pos);
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f173(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == 'l') {
                return f174(cod, ++pos);
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f174(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == 'o') {
                return f175(cod, ++pos);
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f175(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == 's') {
                return f176(cod, ++pos);
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f176(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == 'i') {
                return f177(cod, ++pos);
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f177(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == ' ' || cod.charAt(pos) == '\t') {
                return f170(cod, ++pos);
            }
            if (cod.charAt(pos) == '(') {
                return f178(cod, ++pos);
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f178(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == ' ' || cod.charAt(pos) == '\t') {
                return f178(cod, ++pos);
            }
            if (Character.isLetter(cod.charAt(pos))) {
                strAux = cod.charAt(pos) + "";
                return f179(cod, ++pos);
            }
            if (Character.isDigit(cod.charAt(pos))) {
                return f178(cod, ++pos);
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f179(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == ' ' || cod.charAt(pos) == '\t') {
                return f170(cod, ++pos);
            }
            if (cod.charAt(pos) == '(') {
                return f177(cod, ++pos);
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    //te brinco
    private static int f200(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == ':') {
                strAux2 += "\r\n";
                return f9(cod, ++pos);
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    //testa
    private static int f240(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == 'e') {
                return f241(cod, ++pos);
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f241(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == 's') {
                return f242(cod, ++pos);
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f242(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == 't') {
                return f243(cod, ++pos);
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f243(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == 'a') {
                return f244(cod, ++pos);
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f244(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == ' ' || cod.charAt(pos) == '\t') {
                return f245(cod, ++pos);
            }
            if (cod.charAt(pos) == '\r' || cod.charAt(pos) == '\n') {
                return -11;
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f245(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == ' ' || cod.charAt(pos) == '\t') {
                return f245(cod, ++pos);
            }
            if (Character.isLetter(cod.charAt(pos))) {
                strAux = "" + cod.charAt(pos);
                return f246(cod, ++pos);
            }

            if (cod.charAt(pos) == ':' || cod.charAt(pos) == '=') {
                return -17;
            }

            if (cod.charAt(pos) == '\r' || cod.charAt(pos) == '\n') {
                return -11;
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f246(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (Character.isLetterOrDigit(cod.charAt(pos))) {
                strAux += cod.charAt(pos);
                return f246(cod, ++pos);
            }
            if (cod.charAt(pos) == ' ' || cod.charAt(pos) == '\t') {
                testa aux = new testa(strAux);
                getVariables().add(aux);
                return f247(cod, ++pos);
            }
            if (cod.charAt(pos) == ':') {
                testa aux = new testa(strAux);
                getVariables().add(aux);
                return f9(cod, ++pos);
            }
            if (cod.charAt(pos) == '=') {
                testa aux = new testa(strAux);
                getVariables().add(aux);
                return f248(cod, ++pos);
            }
            if (cod.charAt(pos) == '\r' || cod.charAt(pos) == '\n') {
                return -11;
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f247(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == ' ' || cod.charAt(pos) == '\t') {
                return f247(cod, ++pos);
            }
            if (cod.charAt(pos) == ':') {
                return f9(cod, ++pos);
            }
            if (cod.charAt(pos) == '=') {
                return f248(cod, ++pos);
            }
            if (cod.charAt(pos) == '\r' || cod.charAt(pos) == '\n') {
                return -11;
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f248(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == ' ' || cod.charAt(pos) == '\t') {
                return f248(cod, ++pos);
            }
            if (cod.charAt(pos) == '"') {
                strAux = "";
                return f249(cod, ++pos);
            }
            if (cod.charAt(pos) == '\r' || cod.charAt(pos) == '\n') {
                return -12;
            }
            return -13;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f249(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == '"') {
                GuardarValor(strAux);
                return f250(cod, ++pos);
            }
            if (cod.charAt(pos) == '\r' || cod.charAt(pos) == '\n') {
                return -13;
            }
            strAux += cod.charAt(pos);
            return f249(cod, ++pos);
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f250(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == ' ' || cod.charAt(pos) == '\t') {
                return f250(cod, ++pos);
            }
            if (cod.charAt(pos) == ':') {
                return f9(cod, ++pos);
            }
            if (cod.charAt(pos) == '\r' || cod.charAt(pos) == '\n') {
                return -12;
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    //urgido
    private static int f380(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == 'r') {
                return f381(cod, ++pos);
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f381(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == 'g') {
                return f382(cod, ++pos);
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f382(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == 'i') {
                return f383(cod, ++pos);
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f383(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == 'd') {
                return f384(cod, ++pos);
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f384(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == 'o') {
                return f385(cod, ++pos);
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f385(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == '(') {
                return f386(cod, ++pos);
            }
            if (cod.charAt(pos) == ' ' || cod.charAt(pos) == '\t') {
                return f385(cod, ++pos);
            }
            if (cod.charAt(pos) == '\r' || cod.charAt(pos) == '\n') {
                return -11;
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f386(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (Character.isLetter(cod.charAt(pos))) {
                strAux = cod.charAt(pos) + "";
                return f387(cod, ++pos);
            }
            if (cod.charAt(pos) == ' ' || cod.charAt(pos) == '\t') {
                return f386(cod, ++pos);
            }
            if (cod.charAt(pos) == '\r' || cod.charAt(pos) == '\n') {
                return -11;
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f387(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (Character.isLetterOrDigit(cod.charAt(pos))) {
                strAux += cod.charAt(pos);
                return f387(cod, ++pos);
            }
            if (cod.charAt(pos) == ')') {
                if (ExisteVariable(strAux) > 0) {
                    try {
                        nAux = Integer.parseInt(ObtenValor(strAux));
                        return f389(cod, ++pos);
                    } catch (Exception err) {
                        return -5;
                    }
                } else {
                    return -10;
                }
            }
            if (cod.charAt(pos) == ' ' || cod.charAt(pos) == '\t') {
                return f388(cod, ++pos);
            }
            if (cod.charAt(pos) == '\r' || cod.charAt(pos) == '\n') {
                return -11;
            }

            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f388(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == ')') {
                if (ExisteVariable(strAux) > 0) {
                    try {
                        nAux = Integer.parseInt(ObtenValor(strAux));
                    } catch (Exception err) {
                        return -5;
                    }
                }
                return f389(cod, ++pos);
            }
            if (cod.charAt(pos) == ' ' || cod.charAt(pos) == '\t') {
                return f388(cod, ++pos);
            }
            if (cod.charAt(pos) == '\r' || cod.charAt(pos) == '\n') {
                return -11;
            }

            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f389(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == '.') {
                cU++;
                return f9(cod, ++pos);
            }
            if (cod.charAt(pos) == ' ' || cod.charAt(pos) == '\t') {
                return f389(cod, ++pos);
            }
            if (cod.charAt(pos) == '\r' || cod.charAt(pos) == '\n') {
                return -19;
            }

            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    //Disponible
    private static int f420(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == 'i') {
                return f421(cod, ++pos);
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f421(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == 's') {
                return f422(cod, ++pos);
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f422(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == 'p') {
                return f423(cod, ++pos);
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f423(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == 'o') {
                return f424(cod, ++pos);
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f424(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == 'n') {
                return f425(cod, ++pos);
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f425(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == 'i') {
                return f426(cod, ++pos);
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f426(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == 'b') {
                return f427(cod, ++pos);
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f427(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == 'l') {
                return f428(cod, ++pos);
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f428(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == 'e') {
                return f429(cod, ++pos);
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f429(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == ' ' || cod.charAt(pos) == '\t') {
                return f430(cod, ++pos);
            }
            if (cod.charAt(pos) == '\r' || cod.charAt(pos) == '\n') {
                return -20;
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f430(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (Character.isDigit(cod.charAt(pos))) {
                strAux = cod.charAt(pos) + "";
                return f431(cod, ++pos);
            }
            if (cod.charAt(pos) == ' ' || cod.charAt(pos) == '\t') {
                return f430(cod, ++pos);
            }
            if (cod.charAt(pos) == '\r' || cod.charAt(pos) == '\n') {
                return -20;
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f431(String cod, int pos) {
        cC++;
        cCC++;
        try {

            if (Character.isDigit(cod.charAt(pos))) {
                strAux += cod.charAt(pos);
                return f431(cod, ++pos);
            }
            if (cod.charAt(pos) == ' ' || cod.charAt(pos) == '\t') {
                return f432(cod, ++pos);
            }
            if (cod.charAt(pos) == ':') {
                if (nAux == Integer.parseInt(strAux)) {
                    cD = true;
                    return f9(cod, ++pos);
                } else {
                    int posAux = 0;
                    while (cod.charAt(pos + posAux) != '.') {
                        posAux++;
                        cC++;
                        cCC++;
                    }
                    cD2 = true;
                    return f9(cod, pos += posAux);
                }

            }
            if (cod.charAt(pos) == '\r' || cod.charAt(pos) == '\n') {
                return -12;
            }
            return -9;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f432(String cod, int pos) {
        cC++;
        cCC++;
        try {

            if (cod.charAt(pos) == ' ' || cod.charAt(pos) == '\t') {
                return f432(cod, ++pos);
            }
            if (cod.charAt(pos) == ':') {
                if (nAux == Integer.parseInt(strAux)) {
                    cD = true;
                    cD2 = false;
                    return f9(cod, ++pos);
                } else {

                    if (cod.charAt(pos) == '\r' || cod.charAt(pos) == '\n') {
                        return -12;
                    }
                    int posAux = 0;
                    while (cod.charAt(pos) != '.') {
                        posAux++;
                    }
                    cD = false;
                    cD2 = true;
                    return f9(cod, pos += posAux);
                }

            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    // no pudo
    private static int f463(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == 'u') {
                return f464(cod, ++pos);
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f464(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == 'd') {
                return f465(cod, ++pos);
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f465(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == 'o') {
                return f466(cod, ++pos);
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f466(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == ' ' || cod.charAt(pos) == '\t') {
                return f466(cod, ++pos);
            }
            if (cod.charAt(pos) == ':') {
                if (iscD()) {
                    return f9(cod, ++pos);
                }
                return -3;
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    //Y te vienes..
    private static int f3000(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == '.') {
                return f3001(cod, ++pos);
            }
            return -3;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f3001(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == 'y') {
                return f3002(cod, ++pos);
            }
            return -3;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f3002(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == ' ' || cod.charAt(pos) == '\t') {
                return f3003(cod, ++pos);
            }
            return -3;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f3003(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == 't') {
                return f3004(cod, ++pos);
            }
            return -3;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f3004(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == 'e') {
                return f3005(cod, ++pos);
            }
            return -3;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f3005(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == ' ' || cod.charAt(pos) == '\t') {
                return f3006(cod, ++pos);
            }
            return -3;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f3006(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == 'v') {
                return f3007(cod, ++pos);
            }
            return -3;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f3007(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == 'i') {
                return f3008(cod, ++pos);
            }
            return -3;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f3008(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == 'e') {
                return f3009(cod, ++pos);
            }
            return -3;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f3009(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == 'n') {
                return f3010(cod, ++pos);
            }
            return -3;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static int f3010(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == 'e') {
                return f3011(cod, ++pos);
            }
            return -1;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -3;
        }
    }

    private static int f3011(String cod, int pos) {
        cC++;
        cCC++;
        try {
            if (cod.charAt(pos) == 's') {
                return 1;
            }
            return -3;
        } catch (IndexOutOfBoundsException err) {
            //////System.out.println(err.getMessage());
            return -1;
        }
    }

    private static void salto() {
        cL++;
        cC = 0;
        ////System.out.println("Salto");
    }

    private static boolean valida() {
        if (!iscD() || getcC() == 0 || getcL() == 0 || getcS() == 0 || getcSS() == 0 || getcU() == 0) {
            return true;
        }
        return false;
    }

    static private int GuardarValor(String val) {
        try {
            //DeString dsAux = new DeString();
            int ultimo = getVariables().size() - 1;
            Object o = getVariables().get(ultimo);
            if (o instanceof testa) {
                //////System.out.println("testa");
                testa aux = (testa) o;
                aux.setValor(val);
                ds.setSeparadorMemoria(aux.getNombre());
            ////System.out.println("guardando "+ aux.getNombre()+ " -> " + val);
                getVariables().set(ultimo, aux);
            }
            if (o instanceof entera) {
                System.out.println("entera " + val);
                entera aux = (entera) o;
                aux.setValor(Integer.parseInt(val));
                ds.setSeparadorMemoria(aux.getNombre());
            ////System.out.println("guardando "+ aux.getNombre()+ " -> " + val);
                getVariables().set(ultimo, aux);
            }
            if (o instanceof enpieza) {
                System.out.println("enpieza");
                enpieza aux = (enpieza) o;
                aux.setValor(Float.parseFloat(val));
                ds.setSeparadorMemoria(aux.getNombre());
            ////System.out.println("guardando "+ aux.getNombre()+ " -> " + val);
                getVariables().set(ultimo, aux);
            }
        } catch (Exception err) {
            return -4;
        }
        return 1;
    }

    static private int Guarda(String nombre, String valor) {
        try {
            ////System.out.println(nombre + "  -  " + valor);
            int x = 0;
            for (Object o : getVariables()) {
                if (o instanceof testa) {
                    ////System.out.println("Testa");
                    testa aux = (testa) o;
                    if (aux.getNombre().equals(nombre)) {
                        aux.setValor(valor);
                        if (valor.charAt(0) == '"' && valor.charAt(valor.length() - 1) == '"') {
                            ////System.out.println("debug   " + valor);
                            Variables.set(x, aux);
                            return 1;
                        } else {
                            return -13;
                        }
                    }
                }
                if (o instanceof entera) {
                    ////System.out.println("Entera");
                    entera aux = (entera) o;
                    if (aux.getNombre().equals(nombre)) {
                        ////System.out.println("super");
                        //DeString ds = new DeString(valor);
                        ds.nuevaOperacion(valor);
                        ////System.out.println("as");
                        int res = (int)ds.getResultadoInt();
                        aux.setValor(res);
                        Variables.set(x, aux);
                        return 1;
                    }
                }

                if (o instanceof enpieza) {
                    ////System.out.println("Enpieza");
                    enpieza aux = (enpieza) o;
                    if (aux.getNombre().equals(nombre)) {
                        ds.nuevaOperacion(valor);
                        aux.setValor(ds.getResultado());
                        Variables.set(x, aux);
                        return 1;
                    }
                }
                x++;
            }
        } catch (Exception error) {
            ////System.out.println(error.getMessage());
            ////System.out.println(error.toString());
            return -11;
        }
        return -10;
    }

    static private int ExisteVariable(String nombre) {
        for (Object o : getVariables()) {
            if (o instanceof testa) {
                testa aux = (testa) o;
                if (aux.getNombre().equals(nombre)) {
                    return 1;
                }
            }
            if (o instanceof entera) {
                entera aux = (entera) o;
                if (aux.getNombre().equals(nombre)) {
                    return 1;
                }
            }

            if (o instanceof enpieza) {
                enpieza aux = (enpieza) o;
                if (aux.getNombre().equals(nombre)) {
                    return 1;
                }
            }
        }
        return -10;
    }

    static private String ObtenValor(String nombre) {
        for (Object o : getVariables()) {
            if (o instanceof testa) {
                testa aux = (testa) o;
                if (aux.getNombre().equals(nombre)) {
                    return aux.getValor();
                }
            }
            if (o instanceof entera) {
                entera aux = (entera) o;
                //////System.out.println(aux.getNombre() + "  " + aux.getValor());
                if (aux.getNombre().equals(nombre)) {
                    return aux.getValor() + "";
                }
            }

            if (o instanceof enpieza) {
                enpieza aux = (enpieza) o;
                if (aux.getNombre().equals(nombre)) {
                    return aux.getValor() + "";
                }
            }
        }
        return "";
    }

    static private int TipoVariable(String nombre) {
        for (Object o : getVariables()) {
            if (o instanceof testa) {
                testa aux = (testa) o;
                if (aux.getNombre().equals(nombre)) {
                    return 1;
                }
            }
            if (o instanceof entera) {
                entera aux = (entera) o;
                //////System.out.println(aux.getNombre() + "  " + aux.getValor());
                if (aux.getNombre().equals(nombre)) {
                    return 2;
                }
            }

            if (o instanceof enpieza) {
                enpieza aux = (enpieza) o;
                if (aux.getNombre().equals(nombre)) {
                    return 3;
                }
            }
        }
        return 0;
    }
    
    
    
    public static ArrayList retornaMemoria(){
        
        return ds.getMemoria();
        
    }
    
    
    public static String retornaTexto(){
        return ds.getTripletas();
    }
    
}
