
public class enpieza {
    private String nombre;
    private float valor;
    
    public enpieza(String nombre){
        setNombre(nombre);
    }
    
    public enpieza(String nombre,float valor){
        setNombre(nombre);
        setValor(valor);
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getValor() {
        return valor;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }
    
}
