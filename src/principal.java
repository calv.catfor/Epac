
import Pantallas.PAbrir;
import java.io.File;
import javax.swing.*;

public class principal extends javax.swing.JFrame {

    JFileChooser seleccionado = new JFileChooser();
    File archivo;
    ManejoCompilador manejo = new ManejoCompilador();
    Nucleo n;
    public principal() {

        initComponents();
        jLabel2.setText("");
        n = new Nucleo();
        //setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    /**
     *
     * @param archivo
     * @return
     */
    @SuppressWarnings("unchecked")

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenuBar2 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();
        jScrollPane1 = new javax.swing.JScrollPane();
        Texto = new javax.swing.JEditorPane();
        BAbrir = new javax.swing.JButton();
        BGuardar = new javax.swing.JButton();
        BCopiar = new javax.swing.JButton();
        BCortar = new javax.swing.JButton();
        BPegar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        BNuevo = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        MAbrir = new javax.swing.JMenu();
        OpNuevo = new javax.swing.JMenuItem();
        OpAbrir = new javax.swing.JMenuItem();
        OpGuardarcomo = new javax.swing.JMenuItem();
        OpSalir = new javax.swing.JMenuItem();
        MEditar = new javax.swing.JMenu();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        OpCopiar = new javax.swing.JMenuItem();
        OpCortar = new javax.swing.JMenuItem();
        OpPegar = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        OpSelect = new javax.swing.JMenuItem();
        MFormato = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        Mayuda = new javax.swing.JMenu();
        OpAyuda = new javax.swing.JMenuItem();
        OpAcerca = new javax.swing.JMenuItem();

        jMenu1.setText("File");
        jMenuBar2.add(jMenu1);

        jMenu2.setText("Edit");
        jMenuBar2.add(jMenu2);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        Texto.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                TextoKeyTyped(evt);
            }
        });
        jScrollPane1.setViewportView(Texto);

        BAbrir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BAbrirActionPerformed(evt);
            }
        });

        BGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/guardar-a.png"))); // NOI18N
        BGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BGuardarActionPerformed(evt);
            }
        });

        BCopiar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pantallas/paste-a.png"))); // NOI18N
        BCopiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BCopiarActionPerformed(evt);
            }
        });

        BCortar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pantallas/cortar-a.png"))); // NOI18N
        BCortar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BCortarActionPerformed(evt);
            }
        });

        BPegar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pantallas/copy_a.png"))); // NOI18N
        BPegar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BPegarActionPerformed(evt);
            }
        });

        jLabel1.setText("jLabel1");

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel2.setText("lololo");

        jTextArea1.setEditable(false);
        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane2.setViewportView(jTextArea1);

        BNuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pantallas/new-a.png"))); // NOI18N
        BNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BNuevoActionPerformed(evt);
            }
        });

        MAbrir.setText("Archivo");

        OpNuevo.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
        OpNuevo.setText("Nuevo");
        OpNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                OpNuevoActionPerformed(evt);
            }
        });
        MAbrir.add(OpNuevo);

        OpAbrir.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_A, java.awt.event.InputEvent.CTRL_MASK));
        OpAbrir.setText("Abrir");
        OpAbrir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                OpAbrirActionPerformed(evt);
            }
        });
        MAbrir.add(OpAbrir);

        OpGuardarcomo.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F12, 0));
        OpGuardarcomo.setText("Guardar como...");
        OpGuardarcomo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                OpGuardarcomoActionPerformed(evt);
            }
        });
        MAbrir.add(OpGuardarcomo);

        OpSalir.setText("Salir");
        OpSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                OpSalirActionPerformed(evt);
            }
        });
        MAbrir.add(OpSalir);

        jMenuBar1.add(MAbrir);

        MEditar.setText("Editar");
        MEditar.add(jSeparator1);

        OpCopiar.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.CTRL_MASK));
        OpCopiar.setText("Copiar");
        OpCopiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                OpCopiarActionPerformed(evt);
            }
        });
        MEditar.add(OpCopiar);

        OpCortar.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_X, java.awt.event.InputEvent.CTRL_MASK));
        OpCortar.setText("Cortar");
        OpCortar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                OpCortarActionPerformed(evt);
            }
        });
        MEditar.add(OpCortar);

        OpPegar.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_V, java.awt.event.InputEvent.CTRL_MASK));
        OpPegar.setText("Pegar");
        OpPegar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                OpPegarActionPerformed(evt);
            }
        });
        MEditar.add(OpPegar);
        MEditar.add(jSeparator2);

        OpSelect.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_MASK));
        OpSelect.setText("Seleccionar Todo");
        OpSelect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                OpSelectActionPerformed(evt);
            }
        });
        MEditar.add(OpSelect);

        jMenuBar1.add(MEditar);

        MFormato.setText("Runrun");

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F6, java.awt.event.InputEvent.SHIFT_MASK));
        jMenuItem1.setText("Compilar");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        MFormato.add(jMenuItem1);

        jMenuBar1.add(MFormato);

        Mayuda.setText("About");

        OpAyuda.setText("Ayuda");
        OpAyuda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                OpAyudaActionPerformed(evt);
            }
        });
        Mayuda.add(OpAyuda);

        OpAcerca.setText("Acerca de");
        OpAcerca.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                OpAcercaActionPerformed(evt);
            }
        });
        Mayuda.add(OpAcerca);

        jMenuBar1.add(Mayuda);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 934, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel2))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(BNuevo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(BAbrir, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(BGuardar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(BCopiar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(BCortar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(BPegar)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(BGuardar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(BCopiar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(BCortar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(BPegar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(BAbrir, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(BNuevo))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 389, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void OpSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_OpSalirActionPerformed
        System.exit(0);
    }//GEN-LAST:event_OpSalirActionPerformed

    private void OpAbrirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_OpAbrirActionPerformed
        if (seleccionado.showDialog(this, "Abrir Archivo") == JFileChooser.APPROVE_OPTION) {
            archivo = seleccionado.getSelectedFile();
            if (archivo.canRead()) {
                if (archivo.getName().endsWith("txt")) {
                    String contenido = manejo.AbrirTexto(archivo);
                    Texto.setText(contenido);
                    for (int i = 0; i < contenido.length(); i++) {
                        //System.out.println(contenido.charAt(i) + "  -  " + (int) contenido.charAt(i) + "  -  " + Character.getNumericValue(contenido.charAt(i)));
                    }
                }

            }
        }
        jLabel2.setText("Archivo Cargado");
    }//GEN-LAST:event_OpAbrirActionPerformed

    private void OpCopiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_OpCopiarActionPerformed
        Texto.copy();
    }//GEN-LAST:event_OpCopiarActionPerformed

    private void OpNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_OpNuevoActionPerformed
        Texto.setText("");
        jLabel2.setText("Nuevo");
    }//GEN-LAST:event_OpNuevoActionPerformed

    private void BCopiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BCopiarActionPerformed
        Texto.copy();
    }//GEN-LAST:event_BCopiarActionPerformed

    private void BCortarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BCortarActionPerformed
        Texto.cut();
    }//GEN-LAST:event_BCortarActionPerformed

    private void BPegarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BPegarActionPerformed
        Texto.paste();
    }//GEN-LAST:event_BPegarActionPerformed

    private void OpCortarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_OpCortarActionPerformed
        Texto.cut();
    }//GEN-LAST:event_OpCortarActionPerformed

    private void OpPegarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_OpPegarActionPerformed
        Texto.paste();
    }//GEN-LAST:event_OpPegarActionPerformed

    private void OpSelectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_OpSelectActionPerformed
        Texto.selectAll();
    }//GEN-LAST:event_OpSelectActionPerformed

    private void BGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BGuardarActionPerformed
        if (seleccionado.showDialog(null, "Guardar Archivo") == JFileChooser.APPROVE_OPTION) {
            archivo = seleccionado.getSelectedFile();
            if (archivo.getName().endsWith("txt")) {
                String contenido = Texto.getText();
                String respuesta = manejo.GuardarATexto(archivo, contenido);
                if (respuesta != null) {
                    JOptionPane.showMessageDialog(null, respuesta);
                } else {
                    JOptionPane.showMessageDialog(null, "Error al guardar texto");
                }
            } else {
                JOptionPane.showMessageDialog(null, "El archivo debe guardar se en un formato de texto");
            }
        }
    }//GEN-LAST:event_BGuardarActionPerformed

    private void OpGuardarcomoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_OpGuardarcomoActionPerformed
        if (seleccionado.showDialog(null, "Guardar Archivo") == JFileChooser.APPROVE_OPTION) {
            archivo = seleccionado.getSelectedFile();
            if (archivo.getName().endsWith("txt")) {
                String contenido = Texto.getText();
                String respuesta = manejo.GuardarATexto(archivo, contenido);
                if (respuesta != null) {
                    JOptionPane.showMessageDialog(null, respuesta);
                } else {
                    JOptionPane.showMessageDialog(null, "Error al guardar texto");
                }
            } else {
                JOptionPane.showMessageDialog(null, "El archivo debe guardar se en un formato de texto");
            }
        }
        jLabel2.setText("Guardado");
    }//GEN-LAST:event_OpGuardarcomoActionPerformed

    private void TextoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TextoKeyTyped
        int aux = 0;
        int con = 0;
        while (con < Texto.getText().length()) {
            if (Texto.getText().charAt(con++) == '\n') {
                aux++;
            }
        }
        jLabel1.setText("Saltos de linea : " + aux);
    }//GEN-LAST:event_TextoKeyTyped

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        int codigo = 0;
        if (Texto.getText().length() >= "calienta...y te vienes".length()) {
            if (Texto.getText().contains("calienta.") && Texto.getText().contains("..y te vienes")) {
                codigo = n.f0(Texto.getText());
            } else {
                if (Texto.getText().contains("calienta.")) {
                    codigo = -3;
                } else {
                    codigo = -4;
                }
            }
            //System.out.println(codigo);
        } else {
            codigo = -2;
        }

        switch (codigo) {
            case 1:
                JOptionPane.showMessageDialog(null, "Compilado!");
                //jTextArea1.setText(n.getStrAux2());
                jTextArea1.setText(n.retornaTexto() + "");
                jLabel2.setText("Compilado Exitosamente");
                break;
            case -1:
                JOptionPane.showMessageDialog(null, "Error de sintaxis");
                jLabel2.setText("Llegó hasta Linea " + n.getcL() + "   Columna " + n.getcC());
                try {
                    if (Texto.getText().charAt(n.getcCC()) == '\r' || Texto.getText().charAt(n.getcCC()) == '\n') {
                        Texto.select(n.getcCC() - 2, n.getcCC() + 1);
                    } else {
                        Texto.select(n.getcCC(), n.getcCC() + 1);
                    }
                } catch (Exception r) {

                }
                break;
            case -2:
                JOptionPane.showMessageDialog(null, "No hay suficiente código fuente para empezar a compilar");
                jLabel2.setText("Llegó hasta Linea " + n.getcL() + "   Columna " + n.getcC());
                try {
                    if (Texto.getText().charAt(n.getcCC()) == '\r' || Texto.getText().charAt(n.getcCC()) == '\n') {
                        Texto.select(n.getcCC() - 2, n.getcCC() + 1);
                    } else {
                        Texto.select(n.getcCC(), n.getcCC() + 1);
                    }
                } catch (Exception r) {

                }
                break;
            case -3:
                JOptionPane.showMessageDialog(null, "Faltó o no se escribió correctamente \"..y te vienes\"");
                jLabel2.setText("");
                break;
            case -4:
                JOptionPane.showMessageDialog(null, "Faltó o no se escribió correctamente \"calienta.\"");
                jLabel2.setText("");
                break;
            case -5:
                JOptionPane.showMessageDialog(null, "No hay suficiente código fuente para terminar de compilar o no se ha cerrado debidamente");
                jLabel2.setText("Llegó hasta Linea " + n.getcL() + "   Columna " + n.getcC());
                try {
                    if (Texto.getText().charAt(n.getcCC()) == '\r' || Texto.getText().charAt(n.getcCC()) == '\n') {
                        Texto.select(n.getcCC() - 2, n.getcCC() + 1);
                    } else {
                        Texto.select(n.getcCC(), n.getcCC() + 1);
                    }
                } catch (Exception r) {

                }
                break;
            case -6:
                JOptionPane.showMessageDialog(null, "Posiblemente te ha faltado escribir un parentesis");
                jLabel2.setText("Llegó hasta Linea " + n.getcL() + "   Columna " + n.getcC());
                try {
                    if (Texto.getText().charAt(n.getcCC()) == '\r' || Texto.getText().charAt(n.getcCC()) == '\n') {
                        Texto.select(n.getcCC() - 2, n.getcCC() + 1);
                    } else {
                        Texto.select(n.getcCC(), n.getcCC() + 1);
                    }
                } catch (Exception r) {

                }
                break;
            case -7:
                JOptionPane.showMessageDialog(null, "Error de sintaxis, se esperaba un argumento");
                jLabel2.setText("Llegó hasta Linea " + n.getcL() + "   Columna " + n.getcC());
                try {
                    if (Texto.getText().charAt(n.getcCC()) == '\r' || Texto.getText().charAt(n.getcCC()) == '\n') {
                        Texto.select(n.getcCC() - 2, n.getcCC() + 1);
                    } else {
                        Texto.select(n.getcCC(), n.getcCC() + 1);
                    }
                } catch (Exception r) {

                }
                break;
            case -8:
                JOptionPane.showMessageDialog(null, "No se acepta números decimales para este tipo de dato");
                jLabel2.setText("Llegó hasta Linea " + n.getcL() + "   Columna " + n.getcC());
                try {
                    if (Texto.getText().charAt(n.getcCC()) == '\r' || Texto.getText().charAt(n.getcCC()) == '\n') {
                        Texto.select(n.getcCC() - 2, n.getcCC() + 1);
                    } else {
                        Texto.select(n.getcCC(), n.getcCC() + 1);
                    }
                } catch (Exception r) {

                }
                break;
            case -9:
                JOptionPane.showMessageDialog(null, "Solo se aceptan números enteros para \"disponible\"");
                jLabel2.setText("Llegó hasta Linea " + n.getcL() + "   Columna " + n.getcC());
                try {
                    if (Texto.getText().charAt(n.getcCC()) == '\r' || Texto.getText().charAt(n.getcCC()) == '\n') {
                        Texto.select(n.getcCC() - 2, n.getcCC() + 1);
                    } else {
                        Texto.select(n.getcCC(), n.getcCC() + 1);
                    }
                } catch (Exception r) {

                }
                break;
            case -10:
                JOptionPane.showMessageDialog(null, "Variable no encontrada");
                jLabel2.setText("Llegó hasta Linea " + n.getcL() + "   Columna " + n.getcC() + " ");
                try {
                    if (Texto.getText().charAt(n.getcCC()) == '\r' || Texto.getText().charAt(n.getcCC()) == '\n') {
                        Texto.select(n.getcCC() - 2, n.getcCC() + 1);
                    } else {
                        Texto.select(n.getcCC(), n.getcCC() + 1);
                    }
                } catch (Exception r) {

                }
                break;
            case -11:
                JOptionPane.showMessageDialog(null, "Acción ilegal");
                jLabel2.setText("Llegó hasta Linea " + n.getcL() + "   Columna " + n.getcC());
                try {
                    if (Texto.getText().charAt(n.getcCC()) == '\r' || Texto.getText().charAt(n.getcCC()) == '\n') {
                        Texto.select(n.getcCC() - 2, n.getcCC() + 1);
                    } else {
                        Texto.select(n.getcCC(), n.getcCC() + 1);
                    }
                } catch (Exception r) {

                }
                break;
            case -12:
                JOptionPane.showMessageDialog(null, "Falta escribir dos puntos | ':'");
                jLabel2.setText("Llegó hasta Linea " + n.getcL() + "   Columna " + n.getcC());
                try {
                    if (Texto.getText().charAt(n.getcCC()) == '\r' || Texto.getText().charAt(n.getcCC()) == '\n') {
                        Texto.select(n.getcCC() - 2, n.getcCC() + 1);
                    } else {
                        Texto.select(n.getcCC(), n.getcCC() + 1);
                    }
                } catch (Exception r) {

                }
                break;
            case -13:
                JOptionPane.showMessageDialog(null, "Falta escribir comillas | '\"'");
                jLabel2.setText("Llegó hasta Linea " + n.getcL() + "   Columna " + n.getcC());
                try {
                    if (Texto.getText().charAt(n.getcCC()) == '\r' || Texto.getText().charAt(n.getcCC()) == '\n') {
                        Texto.select(n.getcCC() - 2, n.getcCC() + 1);
                    } else {
                        Texto.select(n.getcCC(), n.getcCC() + 1);
                    }
                } catch (Exception r) {

                }
                break;
            case -14:
                JOptionPane.showMessageDialog(null, "Error de sintaxis, se esperaba operador de concatenación");
                jLabel2.setText("Llegó hasta Linea " + n.getcL() + "   Columna " + n.getcC());
                try {
                    if (Texto.getText().charAt(n.getcCC()) == '\r' || Texto.getText().charAt(n.getcCC()) == '\n') {
                        Texto.select(n.getcCC() - 2, n.getcCC() + 1);
                    } else {
                        Texto.select(n.getcCC(), n.getcCC() + 1);
                    }
                } catch (Exception r) {

                }
                break;
            case -15:
                JOptionPane.showMessageDialog(null, "Se esperaba nombre para declaración de variable enpieza");
                jLabel2.setText("Llegó hasta Linea " + n.getcL() + "   Columna " + n.getcC());
                try {
                    if (Texto.getText().charAt(n.getcCC()) == '\r' || Texto.getText().charAt(n.getcCC()) == '\n') {
                        Texto.select(n.getcCC() - 2, n.getcCC() + 1);
                    } else {
                        Texto.select(n.getcCC(), n.getcCC() + 1);
                    }
                } catch (Exception r) {

                }
                break;
            case -16:
                JOptionPane.showMessageDialog(null, "Se esperaba nombre para declaración de variable entera");
                jLabel2.setText("Llegó hasta Linea " + n.getcL() + "   Columna " + n.getcC());
                try {
                    if (Texto.getText().charAt(n.getcCC()) == '\r' || Texto.getText().charAt(n.getcCC()) == '\n') {
                        Texto.select(n.getcCC() - 2, n.getcCC() + 1);
                    } else {
                        Texto.select(n.getcCC(), n.getcCC() + 1);
                    }
                } catch (Exception r) {

                }
                break;
            case -17:
                JOptionPane.showMessageDialog(null, "Se esperaba nombre para declaración de variable testa");
                jLabel2.setText("Llegó hasta Linea " + n.getcL() + "   Columna " + n.getcC());
                try {
                    if (Texto.getText().charAt(n.getcCC()) == '\r' || Texto.getText().charAt(n.getcCC()) == '\n') {
                        Texto.select(n.getcCC() - 2, n.getcCC() + 1);
                    } else {
                        Texto.select(n.getcCC(), n.getcCC() + 1);
                    }
                } catch (Exception r) {

                }
                break;
            case -18:
                JOptionPane.showMessageDialog(null, "Se necesita escribir \"??\" para escribir un comentario");
                jLabel2.setText("Llegó hasta Linea " + n.getcL() + "   Columna " + n.getcC());
                try {
                    if (Texto.getText().charAt(n.getcCC()) == '\r' || Texto.getText().charAt(n.getcCC()) == '\n') {
                        Texto.select(n.getcCC() - 2, n.getcCC() + 1);
                    } else {
                        Texto.select(n.getcCC(), n.getcCC() + 1);
                    }
                } catch (Exception r) {

                }
                break;
            case -19:
                JOptionPane.showMessageDialog(null, "Se esperaba un punto | '.'");
                jLabel2.setText("Llegó hasta Linea " + n.getcL() + "   Columna " + n.getcC());
                try {
                    if (Texto.getText().charAt(n.getcCC()) == '\r' || Texto.getText().charAt(n.getcCC()) == '\n') {
                        Texto.select(n.getcCC() - 2, n.getcCC() + 1);
                    } else {
                        Texto.select(n.getcCC(), n.getcCC() + 1);
                    }
                } catch (Exception r) {

                }
                break;
            case -20:
                JOptionPane.showMessageDialog(null, "Se esperaba argumento para disponible");
                jLabel2.setText("Llegó hasta Linea " + n.getcL() + "   Columna " + n.getcC());
                try {
                    if (Texto.getText().charAt(n.getcCC()) == '\r' || Texto.getText().charAt(n.getcCC()) == '\n') {
                        Texto.select(n.getcCC() - 2, n.getcCC() + 1);
                    } else {
                        Texto.select(n.getcCC(), n.getcCC() + 1);
                    }
                } catch (Exception r) {

                }
                break;
            case -21:
                JOptionPane.showMessageDialog(null, "Faltó escribir signo igual | '='");
                jLabel2.setText("Llegó hasta Linea " + n.getcL() + "   Columna " + n.getcC());
                try {
                    if (Texto.getText().charAt(n.getcCC()) == '\r' || Texto.getText().charAt(n.getcCC()) == '\n') {
                        Texto.select(n.getcCC() - 2, n.getcCC() + 1);
                    } else {
                        Texto.select(n.getcCC(), n.getcCC() + 1);
                    }
                } catch (Exception r) {

                }
                break;
            default:
                JOptionPane.showMessageDialog(null, "Error Genérico - Código " + codigo);
                jLabel2.setText("Llegó hasta Linea " + n.getcL() + "   Columna " + n.getcC() + "    No se supone que pasara esto.");
                try {
                    if (Texto.getText().charAt(n.getcCC()) == '\r' || Texto.getText().charAt(n.getcCC()) == '\n') {
                        Texto.select(n.getcCC() - 2, n.getcCC() + 1);
                    } else {
                        Texto.select(n.getcCC(), n.getcCC() + 1);
                    }
                } catch (Exception r) {

                }
                break;
        }
        
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void BAbrirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BAbrirActionPerformed
        if (seleccionado.showDialog(this, "Abrir Archivo") == JFileChooser.APPROVE_OPTION) {
            archivo = seleccionado.getSelectedFile();
            if (archivo.canRead()) {
                if (archivo.getName().endsWith("txt")) {
                    String contenido = manejo.AbrirTexto(archivo);
                    Texto.setText(contenido);
                }

            }
        }
        jLabel2.setText("Archivo Cargado");
    }//GEN-LAST:event_BAbrirActionPerformed

    private void OpAyudaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_OpAyudaActionPerformed
        String Mayuda;
        Mayuda = "Palabras Reservadas\r\n\r\n"
                + "calienta.\r\n"
                + "..y te vienes\r\n"
                + "entera: Valores int\r\n"
                + "enpieza: Valores float\r\n"
                + "testa: Valores string\r\n"
                + "metela: Lectura de valores\r\n"
                + "sacala: Impresion de valores y/o texto\r\n"
                + "urgido: switch\r\n"
                + "disponible: case\r\n"
                + "nopudo: break"
                + "te brinco: Salto de linea\r\n";
//                + "siysolosi: if\r\n"
//                + "sino: else\r\n";

        JOptionPane.showMessageDialog(null, Mayuda);
    }//GEN-LAST:event_OpAyudaActionPerformed

    private void OpAcercaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_OpAcercaActionPerformed
        String Macerca;
        Macerca = "                       Equipo\n\n"
                + "                         EPac\n\n"
                + "                INTEGRANTES\n"
                + "Christian Antonio Palma Hernandez\n"
                + "           Edgar Carrillo Galeana\n"
                + "          Servando Salasar Valle\n"
                + "   Jose Antonio Palma Hernandez";

        JOptionPane.showMessageDialog(null, Macerca);
    }//GEN-LAST:event_OpAcercaActionPerformed

    private void BNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BNuevoActionPerformed
        Texto.setText("");
        jLabel2.setText("Nuevo");
    }//GEN-LAST:event_BNuevoActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        new principal().setVisible(true);

    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BAbrir;
    private javax.swing.JButton BCopiar;
    private javax.swing.JButton BCortar;
    private javax.swing.JButton BGuardar;
    private javax.swing.JButton BNuevo;
    private javax.swing.JButton BPegar;
    private javax.swing.JMenu MAbrir;
    private javax.swing.JMenu MEditar;
    private javax.swing.JMenu MFormato;
    private javax.swing.JMenu Mayuda;
    private javax.swing.JMenuItem OpAbrir;
    private javax.swing.JMenuItem OpAcerca;
    private javax.swing.JMenuItem OpAyuda;
    private javax.swing.JMenuItem OpCopiar;
    private javax.swing.JMenuItem OpCortar;
    private javax.swing.JMenuItem OpGuardarcomo;
    private javax.swing.JMenuItem OpNuevo;
    private javax.swing.JMenuItem OpPegar;
    private javax.swing.JMenuItem OpSalir;
    private javax.swing.JMenuItem OpSelect;
    private javax.swing.JEditorPane Texto;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuBar jMenuBar2;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JTextArea jTextArea1;
    // End of variables declaration//GEN-END:variables
}
