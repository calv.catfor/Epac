
public class entera {
    private String nombre;
    private int valor;

    public entera(String nombre){
        setNombre(nombre);
    }

    public entera(String nombre,int valor){
        setNombre(nombre);
        setValor(valor);
    }
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }
    
}
