
public class testa {
    private String nombre;
    private String valor;

    public testa(String nombre){
        setNombre(nombre);
    }

    public testa(String nombre, String valor){
        setNombre(nombre);
        setValor(valor);
    }
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
    
}
