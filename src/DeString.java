
import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * Esta clase es un modulo creado por Christian alias Catfor con la finalidad de
 * poder hacer operaciones jerarquizadas apartir de un String, soporta
 * potencias, multiplicaciones divisiones, sumas, restas y el uso de parentesis.
 *
 * Colorario: El objetivo al que se oriento esta clase, fue facilitar
 * operaciones y procesos más avanzados tales como la derivación y la
 * integración de ecuaciones.
 */
class DeString {

    private static int contador = 1;
    private ArrayList in, out;
    private static ArrayList mem; //Tiene que ser estatica la lista porque si no se pierde la información de las demás posiciones
    //No obstante, de todas formas se agrega la posición donde se hace la asignación a una variable
    //  eso debería bastar para hacer las separaciones entre operaciones algebraicas que se llegasen a hacer

    //Este es el Constructor de la clase donde se crean las listas para funcionar en el proceso de entrada y salidas de los valores
    //  Principalmente se usa en la clase la variable in para ir creando las posiciones internas de las operaciones a realizar
    //  dichas operaciones se integran en un número predefinido de recursividades para facilidad de la jerarquización de las operaciones
    //  el problema que representa el generar una lista que identifique todas las operaciones realizadas entre las distintas instancias
    //  de esta clase, fue resuelta creando un objeto estatico llamado 'mem', este con la intención de guardar las tripletas.
    public DeString() {
        in = new ArrayList();
        out = new ArrayList();
        mem = new ArrayList();
    }

    public static void main(String[] args) {
        DeString ds = new DeString("2+2+4+8+10");
        DeString ds2 = new DeString("50*2/4+100");
        DeString ds3 = new DeString("1000/4/5*2/2");
//        //System.out.println("==================Inicialización=======================");
//        for (int i = 0; i < mem.size(); i++) {
//            Memoria tri = (Memoria) mem.get(i);
//            if (tri.getOperacion() != '=') {
//                //System.out.println("T" + tri.getPos() + " : " + tri.getP0() + tri.getOperacion() + tri.getP1());
//            } else {
//                //System.out.println("T" + tri.getPos() + " " + tri.getOperacion() + " " + tri.getP0());
//            }
//        }
//        //System.out.println("==================PostProcesado========================");

        //System.out.println(ds.getTripletas());
    }

    public DeString(String str) {
        in = new ArrayList();
        str = str.replaceAll(" ", "");
        StringTokenizer st = new StringTokenizer(str, "^*/+-()", true);//[(][2][+][2][)][*][8]
        while (st.hasMoreTokens()) {
            in.add(st.nextToken());
        }
        while (in.size() > 2) {// Esta parte hace la jerarquía de parentesis
            if (in.indexOf("(") != -1) {
                boolean ter = true;
                int cont = 0, p0 = in.indexOf("("), lp = in.size() - 1;
                for (int i = in.indexOf("("); i < in.size() && ter; i++) {
                    String chk = (String) in.get(i);
                    if (chk.equals("(")) {
                        cont++;
                    } else {
                        //en caso de acabar la cantidad de patentesis
                        //se terminaría el ciclado, caso contrario,
                        //solo se disminuye su contador de parentesis que cierran
                        if (chk.equals(")")) {
                            cont--;
                            if (cont < 1) {
                                ter = false;
                            }
                        }//END ELSE )
                    }//END ELSE (
                    lp = i;
                }//END FOR
                String aux = "";
                for (int i = p0 + 1; i < lp; i++) {
                    aux += (String) in.get(i);
                }//18
                String res = new DeString(aux).getResultadoEnString();
                //[(][2][+][2][)] = [4] lo regresa sin parentesis
                in.set(p0, res); //y sustituye el [4] en la posición de [(]
                // En el FOR elimina lo demas que es [2][+][2][)]
                for (int i = p0 + 1; i <= lp; i++) {
                    in.remove(p0 + 1);
                }
            } else {
                if (in.indexOf("^") != -1) {
                    calcula(in.indexOf("^"), '^');
                } else {
                    if (in.indexOf("/") != -1) {
                        calcula(in.indexOf("/"), '/');
                    } else {
                        if (in.indexOf("*") != -1) {
                            calcula(in.indexOf("*"), '*');
                        } else {
                            if (in.indexOf("-") != -1) {
                                calcula(in.indexOf("-"), '-');
                            } else {
                                if (in.indexOf("+") != -1) {
                                    calcula(in.indexOf("+"), '+');
                                } else {

                                }
                            }
                        }
                    }
                }
            }
        }
        ////System.out.println("t" + contador + ": " + getResultadoEnString());
//        Memoria tripleta = new Memoria();
//        try {//Aquí dado el caso que no exista algo en la lista de posiciones de memoria, truena
//            tripleta.setPos(mem.size());
//            tripleta.setOperacion('=');
//            Memoria tempAux = (Memoria) mem.get(mem.size() - 1);
//            tripleta.setP0(tempAux.getResultado() + "");
//        } catch (Exception err) {//por lo que en el catch asignamos la posicion cero, es decir para la primera posicion de memoria
//            mem = new ArrayList();
//            tripleta.setPos(mem.size());
//            tripleta.setOperacion('=');
//            tripleta.setP0("0");
//        }
//        mem.add(tripleta);
    }

    public void nuevaOperacion(String str){
        //System.out.println(str);
        in = new ArrayList();
        str = str.replaceAll(" ", "");
        StringTokenizer st = new StringTokenizer(str, "^*/+-()", true);//[(][2][+][2][)][*][8]
        while (st.hasMoreTokens()) {
            in.add(st.nextToken());
        }
        while (in.size() > 2) {// Esta parte hace la jerarquía de parentesis
            //System.out.print(".");
            if (in.indexOf("(") != -1) {
                boolean ter = true;
                int cont = 0, p0 = in.indexOf("("), lp = in.size() - 1;
                for (int i = in.indexOf("("); i < in.size() && ter; i++) {
                    String chk = (String) in.get(i);
                    if (chk.equals("(")) {
                        cont++;
                    } else {
                        //en caso de acabar la cantidad de patentesis
                        //se terminaría el ciclado, caso contrario,
                        //solo se disminuye su contador de parentesis que cierran
                        if (chk.equals(")")) {
                            cont--;
                            if (cont < 1) {
                                ter = false;
                            }
                        }//END ELSE )
                    }//END ELSE (
                    lp = i;
                }//END FOR
                String aux = "";
                for (int i = p0 + 1; i < lp; i++) {
                    aux += (String) in.get(i);
                }//18
                String res = new DeString(aux).getResultadoEnString();
                //[(][2][+][2][)] = [4] lo regresa sin parentesis
                in.set(p0, res); //y sustituye el [4] en la posición de [(]
                // En el FOR elimina lo demas que es [2][+][2][)]
                for (int i = p0 + 1; i <= lp; i++) {
                    in.remove(p0 + 1);
                }
            } else {
                if (in.indexOf("^") != -1) {
                    calcula(in.indexOf("^"), '^');
                } else {
                    if (in.indexOf("/") != -1) {
                        calcula(in.indexOf("/"), '/');
                    } else {
                        if (in.indexOf("*") != -1) {
                            calcula(in.indexOf("*"), '*');
                        } else {
                            if (in.indexOf("-") != -1) {
                                calcula(in.indexOf("-"), '-');
                            } else {
                                if (in.indexOf("+") != -1) {
                                    calcula(in.indexOf("+"), '+');
                                } else {

                                }
                            }
                        }
                    }
                }
            }
        }
        ////System.out.println("t" + contador + ": " + getResultadoEnString());
//        Memoria tripleta = new Memoria();
//        try {//Aquí dado el caso que no exista algo en la lista de posiciones de memoria, truena
//            tripleta.setPos(mem.size());
//            tripleta.setOperacion('=');
//            Memoria tempAux = (Memoria) mem.get(mem.size() - 1);
//            tripleta.setP0(tempAux.getResultado() + "");
//        } catch (Exception err) {//por lo que en el catch asignamos la posicion cero, es decir para la primera posicion de memoria
//            mem = new ArrayList();
//            tripleta.setPos(mem.size());
//            tripleta.setOperacion('=');
//            tripleta.setP0("0");
//        }
//        mem.add(tripleta);
    }

    
    private void calcula(int p0, char op) {
        Memoria tripleta = new Memoria();
        try {//Aquí dado el caso que no exista algo en la lista de posiciones de memoria, truena
            tripleta.setPos(mem.size());
        } catch (Exception err) {//por lo que en el catch asignamos la posicion cero, es decir para la primera posicion de memoria
            mem = new ArrayList();
            tripleta.setPos(0);
        }
        tripleta.setOperacion(op);
        tripleta.setP0(in.get(p0 - 1).toString());
        tripleta.setP1(in.get(p0 + 1).toString());
        String aux = "", auxO = "";
        switch (op) {
            case '^':
                if (isFloat((String) in.get(p0 - 1)) && isFloat((String) in.get(p0 + 1))) {
                    aux = (float) Math.pow(Float.parseFloat((String) in.get(p0 - 1)), Float.parseFloat((String) in.get(p0 + 1))) + "";
                } else {
                    aux = (String) in.get(p0 - 1) + (String) in.get(p0) + (String) in.get(p0 + 1);
                }
//                auxO = "t" + (contador++) + ": " + in.get(p0 - 1).toString() + in.get(p0).toString() + in.get(p0 + 1).toString();
//                //System.out.println(auxO);
                in.remove(p0 - 1);
                in.remove(p0 - 1);
                in.set(p0 - 1, aux);
                break;
            case '/':
                if (isFloat((String) in.get(p0 - 1)) && isFloat((String) in.get(p0 + 1))) {
                    aux = Float.parseFloat((String) in.get(p0 - 1)) / Float.parseFloat((String) in.get(p0 + 1)) + "";
                } else {
                    aux = (String) in.get(p0 - 1) + (String) in.get(p0) + (String) in.get(p0 + 1);
                }
//                auxO = "t" + (contador++) + ": " + in.get(p0 - 1).toString() + in.get(p0).toString() + in.get(p0 + 1).toString();
//                //System.out.println(auxO);
                in.remove(p0 - 1);
                in.remove(p0 - 1);
                in.set(p0 - 1, aux);
                break;
            case '*':
                if (isFloat((String) in.get(p0 - 1)) && isFloat((String) in.get(p0 + 1))) {
                    aux = Float.parseFloat((String) in.get(p0 - 1)) * Float.parseFloat((String) in.get(p0 + 1)) + "";
                } else {
                    aux = (String) in.get(p0 - 1) + (String) in.get(p0) + (String) in.get(p0 + 1);
                }
//                auxO = "t" + (contador++) + ": " + in.get(p0 - 1).toString() + in.get(p0).toString() + in.get(p0 + 1).toString();
//                //System.out.println(auxO);
                in.remove(p0 - 1);
                in.remove(p0 - 1);
                in.set(p0 - 1, aux);
                break;
            case '+':
                if (isFloat((String) in.get(p0 - 1)) && isFloat((String) in.get(p0 + 1))) {
                    aux = Float.parseFloat((String) in.get(p0 - 1)) + Float.parseFloat((String) in.get(p0 + 1)) + "";
                } else {
                    aux = (String) in.get(p0 - 1) + (String) in.get(p0) + (String) in.get(p0 + 1);
                }
//                auxO = "t" + (contador++) + ": " + in.get(p0 - 1).toString() + in.get(p0).toString() + in.get(p0 + 1).toString();
//                //System.out.println(auxO);
                in.remove(p0 - 1);
                in.remove(p0 - 1);
                in.set(p0 - 1, aux);
                break;
            case '-':
                if (isFloat((String) in.get(p0 - 1)) && isFloat((String) in.get(p0 + 1))) {
                    aux = Float.parseFloat((String) in.get(p0 - 1)) - Float.parseFloat((String) in.get(p0 + 1)) + "";
                } else {
                    aux = (String) in.get(p0 - 1) + (String) in.get(p0) + (String) in.get(p0 + 1);
                }
//                auxO = "t" + (contador++) + ": " + in.get(p0 - 1).toString() + in.get(p0).toString() + in.get(p0 + 1).toString();
//                //System.out.println(auxO);
                in.remove(p0 - 1);
                in.remove(p0 - 1);
                in.set(p0 - 1, aux);
                break;
        }
        tripleta.setResultado(Float.parseFloat(aux));
        mem.add(tripleta);
    }

    public static boolean valida(String[] cadena) {
        boolean isToken = false;
        for (int i = 0; i < cadena.length - 1; i++) {
            if (esToken(cadena[i].charAt(0)) && esToken(cadena[i + 1].charAt(0))) {
                if (i + 2 < cadena.length) {
                    if (esToken(cadena[i + 2].charAt(0))) {
                        return false;
                    } else {
                        String temp = cadena[i] + cadena[i + 1] + "";
                        switch (temp) {
                            case "++":
                            case "+*":
                            case "+/":
                            case "+^":
                            case "-+":
                            case "-*":
                            case "-/":
                            case "-^":
                            case "*+":
                            case "**":
                            case "*/":
                            case "*^":
                            case "/+":
                            case "/*":
                            case "//":
                            case "/^":
                            case "^+":
                            case "^*":
                            case "^/":
                            case "(*":
                            case "(/":
                            case "(+":
                            case "(^":
                            case "()":
                            case ")(":
                            case "-)":
                            case "+)":
                            case "*)":
                            case "/)":
                            case "^)":
                                return false;
                        }
                    }
                } else {
                    String temp = cadena[i] + cadena[i + 1] + "";
                    switch (temp) {
                        case "++":
                        case "+*":
                        case "+/":
                        case "+^":
                        case "-+":
                        case "-*":
                        case "-/":
                        case "-^":
                        case "*+":
                        case "**":
                        case "*/":
                        case "*^":
                        case "/+":
                        case "/*":
                        case "//":
                        case "/^":
                        case "^+":
                        case "^*":
                        case "^/":

                            return false;
                    }
                }
            }
        }

        return true;
    }

    public static boolean esToken(char c) {
        switch (c) {
            case '+':
            case '-':
            case '*':
            case '/':
            case '^':
                return true;
        }
        return false;
    }

    public String getResultadoEnString() {
        if (in.size() > 1) {
            return ((String) in.get(0)) + ((String) in.get(1));
        }
        return ((String) in.get(0));
    }

    public float getResultado() {
        return Float.parseFloat((String) in.get(0));
    }

    public int getResultadoInt() {
        return Integer.parseInt((String) in.get(0));
    }

    public boolean isFloat(String s) {
        try {
            Float.parseFloat(s);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    private String SimX(String cad) {
        String aux[] = cad.split("x^");
//        StringTokenizer st = new StringTokenizer(cad, "x", true);
        return cad;
    }

    //-----------------
    //Aquí se agrega lo que es el proceso para obtener la lista de memoria
    //  y las vueltas del proceso
    //El aliasMem, hace referencia al nombre de la variable a la que se asignará
    public void setSeparadorMemoria(String aliasMem) {
        Memoria tempaux = new Memoria(aliasMem, '=');
        tempaux.setPos(mem.size());
        tempaux.setP1("T" + (mem.size() - 1));
        mem.add(tempaux);
    }

    public ArrayList getMemoria() {
        return mem;
    }

    public String getTripletas() {
        //System.out.println("----sz " + mem.size());
        String acu = "";
        
        for (int i = 0; i < mem.size(); i++) {
            Memoria m = (Memoria) mem.get(i);
            System.out.println(("T" + m.getPos() + " -> " + m.getP0() + " " + m.getOperacion() + " " + m.getP1()) + "\n");
        }
        
        for (int i = 0; i < mem.size(); i++) {
            Memoria auxMem = (Memoria) mem.get(i);

            for (int j = i + 1; j < mem.size(); j++) {
                Memoria auxMem2 = (Memoria) mem.get(j);
                if (auxMem2.getP0().equals(auxMem.getP0()) && auxMem2.getP1().equals(auxMem.getP1()) && auxMem2.getOperacion() == auxMem.getOperacion()) {
                    mem.remove(j);
                    j--;
                }
            }
        }
        
        for (int i = 0; i < mem.size(); i++) {
            Memoria auxMem = (Memoria) mem.get(i);
            for (int j = i + 1; j < mem.size(); j++) {
                Memoria auxMem2 = (Memoria) mem.get(j);
                try {
                    if (Float.parseFloat(auxMem2.getP0()) == auxMem.getResultado()) {
                        auxMem2.setP0("T" + auxMem.getPos());
                    }else{
                        if (Float.parseFloat(auxMem2.getP1()) == auxMem.getResultado()) {
                            auxMem2.setP1("T" + auxMem.getPos());
                        }
                    }
                } catch (Exception e1) {
                    try {
                        if (Float.parseFloat(auxMem2.getP1()) == auxMem.getResultado()) {
                            auxMem2.setP1("T" + auxMem.getPos());
                        }
                    } catch (Exception e2) {

                    }
                }

            }
        }


        for (int i = 0; i < mem.size(); i++) {
            Memoria m = (Memoria) mem.get(i);
            acu += ("T" + m.getPos() + " -> " + m.getP0() + " " + m.getOperacion() + " " + m.getP1()) + "\n";
        }

        return acu;
    }
}
