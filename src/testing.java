
import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class testing {

    public static void main(String[] args) {
        String txt = "calienta. mas codigo aunque no tenga sentido ..y te vienes";
        if (txt.contains("calienta.") && txt.contains("..y te vienes")) {
                System.out.println("compila");
            }
        System.out.println(txt.contains("calienta."));
        System.out.println(txt.contains("..y te vienes"));
    }
    
    public static void testeo() {
        ArrayList Variables = new ArrayList();
        Variables.add(new enpieza("var1"));
        Variables.add(new enpieza("var2"));
        Variables.add(new enpieza("var3"));
        Variables.add(new enpieza("var4"));
        Variables.add(new enpieza("lol"));
        
        Scanner scn = new Scanner(System.in);
        
        String strAux = "var4";
        boolean f = true;
        for (int i = 0; i < Variables.size() && f; i++) {
            Object Variable = Variables.get(i);
            if (Variable instanceof testa) {
                testa aux = (testa) Variable;
                if(aux.getNombre() == strAux){
                    aux.setValor(scn.nextLine());
                    Variables.set(i, aux);
                    f = false;
                }
            }
            if (Variable instanceof entera) {
                entera aux = (entera) Variable;
                if(aux.getNombre() == strAux){
                    aux.setValor(Integer.parseInt(scn.nextLine()));
                    Variables.set(i, aux);
                    f = false;
                }
            }
            if (Variable instanceof enpieza) {
                enpieza aux = (enpieza) Variable;
                if(aux.getNombre() == strAux){
                    aux.setValor(Float.parseFloat(scn.nextLine()));
                    Variables.set(i, aux);
                    f = false;
                }
            }
        }
        for (int i = 0; i < Variables.size(); i++) {
            System.out.println("-");
            Object Variable = Variables.get(i);
            if (Variable instanceof testa) {
                testa aux = (testa) Variable;
                System.out.println(aux.getValor());
            }
            if (Variable instanceof entera) {
                entera aux = (entera) Variable;
                System.out.println(aux.getValor());
            }
            if (Variable instanceof enpieza) {
                enpieza aux = (enpieza) Variable;
                System.out.println(aux.getValor());
            }
        }
    }

    //Se simplificarán pasos para optimizar código e implementar el análisis de palabras que pudieran ser variables
    //Usando una subcadena para saber si la "palabra" que leerá el automata, pertenece a nuestro diccionario de palabras reservadas
    //Posteriormente de obtener la subcadena, usamos el método "contains" para verificar si contiene la palabra reservada
    public void contiene() {
        //Se declara un texto en String para hacer las pruebas de manera estática de las palabras
        String cad = "enpieza entera metela sacala sisolosi te brinco testa urgido";
        //Iniciamos en la posición cero
        int pos = 0;
        //Revisamos de un golpe, que los siguientes 7 caracteres correspondan para la palabra "enpieza"
        System.out.println(cad.substring(pos, pos + 7).contains("enpieza"));
        //Aumentamos los espacios del recorrido simplificado de la palabra enpieza, en este caso 7
        pos += 7;
        //Aumentamos en uno la variable pos, por el espacio que le sigue
        pos++;
        //Revisamos de un golpe, que los siguientes 6 caracteres correspondan para la palabra "entera"
        System.out.println(cad.substring(pos, pos + 6).contains("entera"));
        //Aumentamos los espacios del recorrido simplificado de la palabra enpieza, en este caso 6
        pos += 6;
        //Aumentamos en uno la variable pos, por el espacio que le sigue
        pos++;
        //Revisamos de un golpe, que los siguientes 6 caracteres correspondan para la palabra "metela"
        System.out.println(cad.substring(pos, pos + 6).contains("metela"));
        //Aumentamos los espacios del recorrido simplificado de la palabra enpieza, en este caso 6
        pos += 6;
        //Aumentamos en uno la variable pos, por el espacio que le sigue
        pos++;
        //Revisamos de un golpe, que los siguientes 6 caracteres correspondan para la palabra "sacala"
        System.out.println(cad.substring(pos, pos + 6).contains("sacala"));
        //Aumentamos los espacios del recorrido simplificado de la palabra enpieza, en este caso 6
        pos += 6;
        //Aumentamos en uno la variable pos, por el espacio que le sigue
        pos++;
        //Revisamos de un golpe, que los siguientes 8 caracteres correspondan para la palabra "sisolosi"
        System.out.println(cad.substring(pos, pos + 8).contains("sisolosi"));
        //Aumentamos los espacios del recorrido simplificado de la palabra enpieza, en este caso 8
        pos += 8;
        //Aumentamos en uno la variable pos, por el espacio que le sigue
        pos++;
        //Revisamos de un golpe, que los siguientes 9 caracteres correspondan para la palabra "te brinco"
        System.out.println(cad.substring(pos, pos + 9).contains("te brinco"));
        //Aumentamos los espacios del recorrido simplificado de la palabra enpieza, en este caso 9
        pos += 9;
    }

    public void ejecuta() {
        try {
            //creamos una variable que permitirá la ejecucion del comando
            Runtime rt = Runtime.getRuntime();
            //Ojo, el primer valor, en este caso ping, es el comando, y lo de 8.8.8.8 es/son los parametros que usarían para ejecutar el exec
            String[] commands = {"ping", "8.8.8.8"};
            //Se ejecuta el comando...
            Process proc = rt.exec(commands);
            //Se crean los buffers para leer lo que suceda de parte del proceso que se crea en la linea anterior
            BufferedReader stdInput = new BufferedReader(new InputStreamReader(proc.getInputStream()));
            BufferedReader stdError = new BufferedReader(new InputStreamReader(proc.getErrorStream()));

            // Regresa todo lo que regresaría normalmente en consola
            System.out.println("Aquí esta lo que salio del comando realizado:\n");
            String s = null;
            //Para en caso donde el stdInput, resulte tener respuesta de salida del comando ejecutado, se imprimirán en pantalla, linea por linea
            //para esto se guarda la cadena que se imprimirá en una variable "s" declarada arriba como null, al momento que dejen de existir lineas
            //la variable 's' será null y saldrá del while.
            while ((s = stdInput.readLine()) != null) {
                System.out.println(s);
            }

            // Le los errores manejados desde el programa de consola
            System.out.println("Aquí está la salida de error en caso de que esxita alguno:\n");
            while ((s = stdError.readLine()) != null) {
                System.out.println(s);
            }
        } catch (Exception e) {
            System.out.println("No se ejecuto correctamente el comando u otra situacion:\n" + e.getMessage());
        }
    }
}
